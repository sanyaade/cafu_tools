# Sonnenpositionen zu den SkyDomes von Peter Kleiner
#
# F�r das "Heading"  gilt: Im Ca3DE-Koordinatensystem ist 0 entlang der pos. X-Achse, 90 entlang der pos. Y-Achse.
#                          ACHTUNG: Mit dem Heading eines Entities verh�lt es sich genau umgekehrt!
# F�r die "Altitude" gilt: 0 ist am Horizont, 90 ist senkrecht nach oben.
#                          ACHTUNG: F�r den Pitch eines Entities ist 90 senkrecht nach unten!

use strict;             # turn 'strict' mode on
use warnings;           # -w

# Name, Heading, Altitude  (Werte aus Terragen, via Email von Peter Kleiner).
my @SkyDomes = ( [ 'PK_BrightDay1', -84.611, 39.719 ],
                 [ 'PK_BrightDay2',  91.397, 45.616 ],
                 [ 'PK_Autumn'    , 137.386, 45.000 ],
                 [ 'PK_Winter'    , -60.000, 66.218 ],
                 [ 'PK_Night'     ,  91.081, 31.430 ],
                 [ 'PK_Sunset'    , -86.760, 22.521 ],
                 [ 'PK_Sunset2'   ,  35.961,  7.125 ] );


for (my $sd=0; $sd<=$#SkyDomes; $sd++)
{
    # Die Richtungsvektoren der Sonne errechnen sich wie folgt:
    my $Heading  = $SkyDomes[$sd][1]/180.0*3.1415926;
    my $Altitude = $SkyDomes[$sd][2]/180.0*3.1415926;

    my $x = cos($Altitude) * cos($Heading);
    my $y = cos($Altitude) * sin($Heading);
    my $z = sin($Altitude);

    # Beachte: Wir wollen die Vektoren des Sonnenlichts (von der Sonne in Richtung Boden, nicht umgekehrt),
    # damit wir die ausgedrucketen Zahlen direkt in die Maps �bernehmen k�nnen.
    $x = -$x;
    $y = -$y;
    $z = -$z;

    # Die folgende Zeile war urspr�nglich nicht n�tig, als die SkyDomes vor Subversion Revision 103 noch mit dem DrawableSkyDomeT
    # "per Hand" gezeichnet wurden. Nun (ab Rev. 105) werden sie als echte Cube-Maps gezeichnet, und die �bergangstabelle
    # lf -> px,   ft -> pz,   up -> py (und 90 Grad nach rechts drehen),
    # rt -> nx,   bk -> nz,   dn -> ny (und 90 Grad nach links  drehen)
    # entspricht einer Spiegelung an der YZ-Ebene, daher ist auch eine entsprechende Spiegelung des Sonnenlichtvektors notwendig.
    $x = -$x;

    printf("%2u %-20s Hdg%8.3f  Alt%7.3f  (%8.5f %8.5f %8.5f)\n", $sd, $SkyDomes[$sd][0], $SkyDomes[$sd][1], $SkyDomes[$sd][2], $x, $y, $z);
}
