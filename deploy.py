#!/usr/bin/env python
# -*- coding: utf-8 -*-
import argparse
import paramiko     # If this line causes an error, consult README.txt in the same directory.
import os, shutil, sys
from datetime import date
from common import Info, CopyRepoToBinary


parser = argparse.ArgumentParser(description='Automatically create a binary release of the Cafu Engine for the current platform, and upload it to the Cafu website.')
parser.add_argument("CafuRepo", help="The root of a Cafu source code repository.")
parser.add_argument('--username', action="store", help='the FTP username for uploading the files')
parser.add_argument('--password', action="store", help='the FTP password for uploading the files')

args         = parser.parse_args()
ftp_username = args.username or raw_input("FTP username: ")
ftp_password = args.password or raw_input("FTP password: ")


# This is a very simple test for the newly written functions in `common.py`.
# It essentially duplicates (some of) the functionality in `make_binary.py`,
# but in a manner that is easier to re-use, to maintain, and to adapt to new
# purposes.
# All functionality for updating and uploading the Doxygen reference docs
# has been moved into the main repository's `Doxygen/build.py` script.
# This script should be fixed and augmented to create and upload binary
# releases of Cafu.


version_string = date.today().strftime("%Y-%m-%d")
dest_dir = "Cafu-bin-" + version_string + "-" + sys.platform
if sys.platform != "win32":
    dest_dir += "-" + platform.machine()
print Info("ok:"), '"' + version_string + '"'

# See the documentation of this function in `common.py` for details.
CopyRepoToBinary(args.CafuRepo, dest_dir)

fn = os.path.basename(shutil.make_archive(
    dest_dir,                                       # The name of the file to create, including the path, minus any format-specific extension.
    "zip" if sys.platform == "win32" else "gztar",  # tar (but not zip) preserves file permissions, i.e. the executable bit of our binaries.
    ".",
    dest_dir
))
print Info("ok:"), fn + ",", os.path.getsize(fn), "bytes"


if ftp_username and ftp_password:
    ssh = paramiko.SSHClient()

    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect('ftp.cafu.de', username=ftp_username, password=ftp_password)

    sftp = ssh.open_sftp()

    Upload(sftp, args.CafuRepo + "/Doxygen/cpp/out/html", "cafu/docs/c++")
    Upload(sftp, args.CafuRepo + "/Doxygen/scripting/out/html", "cafu/docs/lua")

    sftp.close()
